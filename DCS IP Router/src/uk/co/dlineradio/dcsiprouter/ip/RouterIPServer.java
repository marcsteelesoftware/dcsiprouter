package uk.co.dlineradio.dcsiprouter.ip;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Level;
import java.util.logging.Logger;

import uk.co.dlineradio.dcsiprouter.settings.Settings;

/**
 * Server for the IP side of things. This interfaces with both external clients
 * and the router in question.
 * @author Marc Steele
 */

public class RouterIPServer implements Runnable {
	
	private Lock thread_control_lock = new ReentrantLock();
	private Thread running_thread = null;
	private boolean run_thread = false;
	private List<RouterIPSessionListener> listeners = new LinkedList<RouterIPSessionListener>();
	
	private static RouterIPServer server = null;
	private static Logger logger = Logger.getLogger(RouterIPServer.class.getName());
	private static final int SLEEP_TIME = 1000;
	private static final int SOCKET_TIMEOUT = 100;
	
	/**
	 * Follows singleton pattern.
	 */
	
	private RouterIPServer() { }
	
	/**
	 * Retrieves the one and only instance of the server.
	 * @return The one and only instance.
	 */
	
	public static RouterIPServer get() {
		
		if (RouterIPServer.server == null) {
			RouterIPServer.server = new RouterIPServer();
		}
		
		return RouterIPServer.server;
		
	}

	@Override
	public void run() {
		
		// Setup the server socket
		
		ServerSocket inboundSocket = null;
		
		try {
			
			inboundSocket = new ServerSocket(Settings.get().getPort());
			inboundSocket.setSoTimeout(SOCKET_TIMEOUT);
			
		} catch (IOException e1) {
			logger.log(Level.SEVERE, String.format("Failed to open the server socket on port %1$d. Bailing out.", Settings.get().getPort()));
			return;
		}
		
		// Tell the user all is good
		
		logger.log(Level.INFO, String.format("Started TCP/IP server on port %1$d", Settings.get().getPort()));
		
		// The running server
		
		while (this.run_thread) {
			
			// Look for inbound connections and create new sessions
			
			try {
				
				Socket connection = inboundSocket.accept();
				if (connection != null) {
					RouterIPSession session = new RouterIPSession(connection, this);
				}
				
			} catch (IOException e) {
				// Silent TIMEOUT!!!
				// logger.log(Level.INFO, "Timout waiting for inbound connections.");
			}
			
			// Sleep it off
			
			try {
				Thread.sleep(SLEEP_TIME);
			} catch (InterruptedException e) {
				logger.log(Level.WARNING, "Interrupted in the master IP server thread.");
			}
			
		}
		
		// Cleanup
		
		try {
			
			inboundSocket.close();
			
		} catch (IOException e) {
			logger.log(Level.WARNING, "Ran into a problem stopping the IP server.", e);
		}
		
	}
	
	/**
	 * Queues a routers command for broadcast to all clients.
	 * @param command The command to queue.
	 */
	
	public void queueOutboundBroadcastCommand(RouterIPCommand command) {
		
		for (RouterIPSessionListener listener:this.listeners) {
			listener.sendCommand(command);
		}
		
	}

	/**
	 * Starts the server thread.
	 * @return TRUE if it was successful. Otherwise FALSE.
	 */
	
	public boolean startThread() {
		
		this.thread_control_lock.lock();
		
		// Check we're not already running
		
		if (this.running_thread != null) {
			this.thread_control_lock.unlock();
			return false;
		}
		
		// Launch the thread
		
		this.run_thread = true;
		this.running_thread = new Thread(this);
		this.running_thread.start();
		
		this.thread_control_lock.unlock();
		return true;
		
	}
	
	/**
	 * Stops the server thread.
	 * @return TRUE if it was successful. Otherwise FALSE.
	 */

	public boolean stopThread() {
		
		this.thread_control_lock.lock();
		
		// Check we're actually running
		
		if (this.running_thread == null) {
			this.thread_control_lock.unlock();
			return false;
		} else {
			this.run_thread = false;
			this.running_thread.interrupt();
			this.running_thread = null;
			this.thread_control_lock.unlock();
			return true;
		}
		
	}
	
	/**
	 * Register a listener for the broadcast commands.
	 * @param listener The listener to register.
	 */
	
	public void registerListener(RouterIPSessionListener listener) {
		
		if (listener != null && !this.listeners.contains(listener)) {
			this.listeners.add(listener);
		}
		
	}
	
	/**
	 * Deregisters a listener from broadcast commands.
	 * @param listener The listener to deregister.
	 */
	
	public void deregisterListener(RouterIPSessionListener listener) {
		
		if (listener != null && this.listeners.contains(listener)) {
			this.listeners.remove(listener);
		}
		
	}
	
}
