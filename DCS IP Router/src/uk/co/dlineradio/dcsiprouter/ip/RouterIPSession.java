package uk.co.dlineradio.dcsiprouter.ip;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.logging.Level;
import java.util.logging.Logger;

import uk.co.dlineradio.dcsiprouter.ip.commands.TXPathCommand;
import uk.co.dlineradio.dcsiprouter.ip.commands.TXRouteCommand;
import uk.co.dlineradio.dcsiprouter.ip.commands.TXSourceCommand;
import uk.co.dlineradio.dcsiprouter.ip.commands.TXStatusCommand;
import uk.co.dlineradio.dcsiprouter.ip.commands.TXToggleCommand;
import uk.co.dlineradio.dcsiprouter.settings.Settings;

/**
 * Handles a session for the router IP server.
 * @author Marc Steele
 */

public class RouterIPSession implements RouterIPSessionListener, Runnable {
	
	private Socket socket = null;
	private RouterIPServer server = null;
	private ConcurrentLinkedQueue<RouterIPCommand> outboundCommandQueue = new ConcurrentLinkedQueue<RouterIPCommand>();
	
	private static Logger logger = Logger.getLogger(RouterIPSession.class.getName());
	private static final int SLEEP_TIME = 100;
	private static Map<String, Class> commandMap = null;
	
	/**
	 * Generates the command map.
	 */
	
	static {
		
		commandMap = new HashMap<String, Class>();
		commandMap.put("sources", TXSourceCommand.class);
		commandMap.put("paths", TXPathCommand.class);
		commandMap.put("switch", TXRouteCommand.class);
		commandMap.put("toggle", TXToggleCommand.class);
		commandMap.put("status", TXStatusCommand.class);
		
	}
	
	
	/**
	 * Creates a new session.
	 * @param socket The socket the session is on.
	 * @param server The server the session is on.
	 */
	
	public RouterIPSession(Socket socket, RouterIPServer server) {
		
		// Sanity check
		
		if (socket == null || server == null) {
			logger.log(Level.WARNING, "We were asked to launch a session but we weren't given a socket or server for this to happen on.");
			return;
		}
		
		// Assignments
		
		this.socket = socket;
		this.server = server;
		
		// Register
		
		server.registerListener(this);
		
		// Launch thread
		
		new Thread(this).start();
		
	}

	@Override
	public void sendCommand(RouterIPCommand command) {
		
		if (command != null) {
			this.outboundCommandQueue.add(command);
		}
		
	}

	@Override
	public void run() {
		
		// Setup the buffered reader/writer
		
		BufferedReader socketIn = null;
		BufferedWriter socketOut = null;
		
		try {
			
			socketIn = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			socketOut = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
			
		} catch (IOException e) {
			logger.log(Level.SEVERE, "Bailing out of client thread as we couldn't setup the reader/writer.", e);
			return;
		}
		
		// Register our listener
		
		this.server.registerListener(this);
		
		// Keep going while we're still connected
		
		while (this.socket != null && this.socket.isConnected()) {
			
			// Punt out any queued commands
			
			try {
				
				while (!this.outboundCommandQueue.isEmpty()) {
					socketOut.write(this.outboundCommandQueue.poll().toCommandString());
				}
				
				socketOut.flush();
				
			} catch (IOException e) {
				logger.log(Level.SEVERE, "Ran into an IO problem punting out queued commands to a client.", e);
			}
			
			// Pull in any inbound commands
			
			try {
				
				String currentLine = null;
				while (socket.getInputStream().available() > 0 && (currentLine = socketIn.readLine()) != null) {
					
					// Pull out the first token and try to work out what the command is
					
					String[] commandTokens = currentLine.split(" ");
					Class commandClass = commandMap.get(commandTokens[0]);
					if (commandClass == null) {
						logger.log(Level.WARNING, String.format("%1$s is not a valid command from a client.", commandTokens[0]));
						continue;
					}
					
					// Instantiate (if we can)
					
					RouterIPCommand inflatedCommand = null;
					
					try {
						
						Method instantiateMethod = commandClass.getDeclaredMethod("fromCommandText", new Class[] {String.class});
						inflatedCommand = (RouterIPCommand) instantiateMethod.invoke(null, currentLine);
						
					} catch (NoSuchMethodException e) {
						logger.log(Level.SEVERE, String.format("Turns out we couldn't inflate the %1$s command as the command is missing the required method.", currentLine), e);
						continue;
					} catch (SecurityException e) {
						logger.log(Level.SEVERE, String.format("Turns out we're not allowed to inflate the %1$s command.", currentLine), e);
						continue;
					} catch (IllegalAccessException e) {
						logger.log(Level.SEVERE, String.format("Turns out we're not allowed to inflate the %1$s command.", currentLine), e);
						continue;
					} catch (IllegalArgumentException e) {
						logger.log(Level.SEVERE, String.format("Oi! Stop sending illegal arguments in the %1$s command.", currentLine), e);
						continue;
					} catch (InvocationTargetException e) {
						logger.log(Level.SEVERE, String.format("We had a serious backend problem inflating the %1$s command.", currentLine), e);
						continue;
					}
					
					if (inflatedCommand == null) {
						logger.log(Level.WARNING, String.format("Ran into a problem inflating the %1$s command from a client.", currentLine));
						continue;
					}
					
					// Process the command
					
					if (inflatedCommand instanceof TXPathCommand || inflatedCommand instanceof TXSourceCommand || inflatedCommand instanceof TXStatusCommand) {
						this.outboundCommandQueue.add(inflatedCommand);
					} else if (inflatedCommand instanceof TXRouteCommand) {
						TXRouteCommand routeCommand = (TXRouteCommand) inflatedCommand;
						Settings.get().getRouter().switchTX(routeCommand.getSource(), routeCommand.getTx_path());
					} else if (inflatedCommand instanceof TXToggleCommand) {
						TXToggleCommand toggleCommand = (TXToggleCommand) inflatedCommand;
						Settings.get().getRouter().toggleSource(toggleCommand.getSource(), toggleCommand.getTx_path(), toggleCommand.isToggle());
					}
					
				}
				
			} catch (IOException e) {
				logger.log(Level.SEVERE, "Ran into an IP problem reading in commands from the client.", e);
			}
			
			// Sleep it off
			
			try {
				Thread.sleep(SLEEP_TIME);
			} catch (InterruptedException e) {
				logger.log(Level.WARNING, "IP client thread interrupted.", e);
			}
			
		}
		
		// Cleanup
		
		this.server.deregisterListener(this);
		
	}

}
