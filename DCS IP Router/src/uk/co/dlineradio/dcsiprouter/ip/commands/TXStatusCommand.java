package uk.co.dlineradio.dcsiprouter.ip.commands;

import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import uk.co.dlineradio.dcsiprouter.ip.RouterIPCommand;
import uk.co.dlineradio.dcsiprouter.router.Router;
import uk.co.dlineradio.dcsiprouter.router.Source;
import uk.co.dlineradio.dcsiprouter.router.Transmitter;
import uk.co.dlineradio.dcsiprouter.settings.Settings;

/**
 * This command allows clients to query the current state of the router.
 * @author Marc Steele
 */

public class TXStatusCommand implements RouterIPCommand {
	
	private static Logger logger = Logger.getLogger(TXStatusCommand.class.getName());

	@Override
	public String toCommandString() {
		
		// Sanity check
		
		Map<Integer,Transmitter> transmitters = Settings.get().getTransmitters();
		if (transmitters == null || transmitters.isEmpty()) {
			logger.log(Level.SEVERE, "Asked to get the current router state for a client but no transmitters were found in the configuration.");
			return null;
		}
		
		Router router = Settings.get().getRouter();
		if (router == null) {
			logger.log(Level.SEVERE, "Asked to get the current router state for a client but we couldn't find the router.");
			return null;
		}
		
		Map<Integer, Source> sources = Settings.get().getSources();
		if (sources == null || sources.isEmpty()) {
			logger.log(Level.WARNING, "Asked to get the current router state for a client by no sources were found in the configuration.");
			return null;
		}
		
		// Build up the command
		
		StringBuilder sb = new StringBuilder();
		
		for (Transmitter currentTransmitter:transmitters.values()) {
			
			Source currentSource = sources.get(router.currentSourceToTX(currentTransmitter.getId()));
			if (currentSource != null && !currentSource.isToggle()) {
				sb.append(String.format("status %1$d %2$d TX\n", currentTransmitter.getId(), currentSource.getId()));
			}
			
		}
		
		return sb.toString();
		
	}
	
	/**
	 * Creates a new instance of the command based on the command text.
	 * @param commandText The command text.
	 */
	
	public static TXStatusCommand fromCommandText(String commandText) {
		
		// Sanity check
		
		if (commandText == null || commandText.isEmpty()) {
			logger.log(Level.WARNING, "Asked to inflate a command from the text but no text was given.");
			return null;
		}
		
		// Perform the inflation
		
		return commandText.startsWith("status") ? new TXStatusCommand() : null;
		
	}

	@Override
	public String toString() {
		return "command requesting the current router state";
	}

}
