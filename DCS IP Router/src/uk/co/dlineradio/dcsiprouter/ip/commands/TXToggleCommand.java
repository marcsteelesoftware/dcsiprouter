package uk.co.dlineradio.dcsiprouter.ip.commands;

import java.util.logging.Level;
import java.util.logging.Logger;

import uk.co.dlineradio.dcsiprouter.ip.RouterIPCommand;

/**
 * The command for toggling a TX source on or off.
 * @author Marc Steele
 */

public class TXToggleCommand implements RouterIPCommand {
	
	private int tx_path = 0;
	private int source = 0;
	private boolean toggle;
	
	private static Logger logger = Logger.getLogger(TXRouteCommand.class.getName());
	private static final int TOKEN_COUNT = 4;
	
	/**
	 * Creates a new instance of the command.
	 * @param tx_path
	 * @param source
	 * @param toggle
	 */
	
	public TXToggleCommand(int tx_path, int source, boolean toggle) {
		this.tx_path = tx_path;
		this.source = source;
		this.toggle = toggle;
	}

	/* (non-Javadoc)
	 * @see uk.co.dlineradio.dcsiprouter.ip.RouterIPCommand#toCommandString()
	 */
	@Override
	public String toCommandString() {
		return String.format("switch %1$d %2$d %3$s\n", this.tx_path, this.source, this.toggle ? "on" : "off");
	}
	
	/**
	 * Obtains the TX path the command is for.
	 * @return The TX path the command is for.
	 */

	public int getTx_path() {
		return tx_path;
	}
	
	/**
	 * Sets the TX path the command is for.
	 * @param tx_path The TX path the command is for.
	 */

	public void setTx_path(int tx_path) {
		this.tx_path = tx_path;
	}
	
	/**
	 * Obtains the source the command is for.
	 * @return The source the command is for.
	 */

	public int getSource() {
		return source;
	}
	
	/**
	 * Sets the source the command is for.
	 * @param source The source the command is for.
	 */

	public void setSource(int source) {
		this.source = source;
	}
	
	/**
	 * Obtains the toggle value for the command.
	 * @return TRUE if the command is to switch the source on.
	 */

	public boolean isToggle() {
		return toggle;
	}
	
	/**
	 * Sets the toggle value for the command.
	 * @param toggle TRUE if the command is to switch the source on.
	 */

	public void setToggle(boolean toggle) {
		this.toggle = toggle;
	}

	@Override
	public String toString() {
		return String.format("command to toggle source %2$d on TX path %1$s to %3$s", this.tx_path, this.source, this.toggle ? "on" : "off");
	}
	
	/**
	 * Creates a new instance of the command based on the command text.
	 * @param commandText The command text.
	 */
	
	public static TXToggleCommand fromCommandText(String commandText) {
		
		// Sanity check
		
		if (commandText == null || commandText.isEmpty()) {
			logger.log(Level.WARNING, "Asked to inflate a command from the text but no text was given.");
			return null;
		}
		
		String[] commandTokens = commandText.split(" ");
		if (commandTokens.length < TOKEN_COUNT) {
			logger.log(Level.WARNING, "Asked to inflate a toggle command but not given enough parameters!");
			return null;
		}
		
		// Perform the inflation
		
		try {
		
			int pathID = Integer.parseInt(commandTokens[1]);
			int sourceID = Integer.parseInt(commandTokens[2]);
			return new TXToggleCommand(pathID, sourceID, commandTokens[3].equals("on"));
		
		} catch (Exception ex) {
			logger.log(Level.WARNING, "Ran into a problem inflating a TX toggle command.", ex);
			return null;
		}
		
	}
	
}
