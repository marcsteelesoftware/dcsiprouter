package uk.co.dlineradio.dcsiprouter.ip.commands;

import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import uk.co.dlineradio.dcsiprouter.ip.RouterIPCommand;
import uk.co.dlineradio.dcsiprouter.router.Transmitter;
import uk.co.dlineradio.dcsiprouter.settings.Settings;

/**
 * The IP command for listing the available routes on the router.
 * This command is only sent to clients and is not processed by the server.
 * @author Marc Steele
 */

public class TXPathCommand implements RouterIPCommand {
	
	private static Logger logger = Logger.getLogger(TXPathCommand.class.getName());

	@Override
	public String toCommandString() {
		
		// Sanity check
		
		Map<Integer,Transmitter> transmitters = Settings.get().getTransmitters();
		if (transmitters == null || transmitters.isEmpty()) {
			logger.log(Level.SEVERE, "Asked to generate a list of transmitters for a client but no transmitters were found in the configuration.");
			return null;
		}
		
		// Generate the command
		
		StringBuilder sb = new StringBuilder();
		sb.append("paths ");
		
		boolean firstTransmitter = true;
		for (Transmitter currentTransmitter:transmitters.values()) {
			if (firstTransmitter) {
				sb.append(String.format("%1$d=%2$s", currentTransmitter.getId(), currentTransmitter.getName()));
				firstTransmitter = false;
			} else {
				sb.append(String.format("|%1$d=%2$s", currentTransmitter.getId(), currentTransmitter.getName()));
			}
		}
		
		sb.append("\n");
		return sb.toString();
		
	}
	
	/**
	 * Creates a new instance of the command based on the command text.
	 * @param commandText The command text.
	 */
	
	public static TXPathCommand fromCommandText(String commandText) {
		
		// Sanity check
		
		if (commandText == null || commandText.isEmpty()) {
			logger.log(Level.WARNING, "Asked to inflate a command from the text but no text was given.");
			return null;
		}
		
		// Perform the inflation
		
		return commandText.startsWith("paths") ? new TXPathCommand() : null;
		
	}

	@Override
	public String toString() {
		return "command requesting the list of TX paths";
	}

}
