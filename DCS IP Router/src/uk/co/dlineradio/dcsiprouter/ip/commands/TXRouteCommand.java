package uk.co.dlineradio.dcsiprouter.ip.commands;

import java.util.logging.Level;
import java.util.logging.Logger;

import uk.co.dlineradio.dcsiprouter.ip.RouterIPCommand;

/**
 * A command sent out over IP indicating that a transmitter has been switched.
 * @author Marc Steele
 */

public class TXRouteCommand implements RouterIPCommand {
	
	private int tx_path = 0;
	private int source = 0;
	
	private static Logger logger = Logger.getLogger(TXRouteCommand.class.getName());
	private static final int TOKEN_COUNT = 3;
	
	/**
	 * Creates a new TX routing command for sending over IP.
	 * @param tx_path The TX path number we've changed.
	 * @param source The source number we've changed it to.
	 */
	
	public TXRouteCommand(int tx_path, int source) {
		this.tx_path = tx_path;
		this.source = source;
	}

	@Override
	public String toCommandString() {
		return String.format("switch %1$d %2$d\n", this.tx_path, this.source);
	}
	
	/**
	 * Obtains the TX path the command is for.
	 * @return The TX path the command is for.
	 */

	public int getTx_path() {
		return tx_path;
	}
	
	/**
	 * Sets the TX path the command is for.
	 * @param tx_path The TX path the command is for.
	 */

	public void setTx_path(int tx_path) {
		this.tx_path = tx_path;
	}
	
	/**
	 * Obtains the source the command is for.
	 * @return The source the command is for.
	 */

	public int getSource() {
		return source;
	}
	
	/**
	 * Sets the source the command is for.
	 * @param source The source the command is for.
	 */

	public void setSource(int source) {
		this.source = source;
	}

	@Override
	public String toString() {
		return String.format("command to switch TX path %1$d to source %2$d", this.tx_path, this.source);
	}
	
	/**
	 * Creates a new instance of the command based on the command text.
	 * @param commandText The command text.
	 */
	
	public static TXRouteCommand fromCommandText(String commandText) {
		
		// Sanity check
		
		if (commandText == null || commandText.isEmpty()) {
			logger.log(Level.WARNING, "Asked to inflate a command from the text but no text was given.");
			return null;
		}
		
		String[] commandTokens = commandText.split(" ");
		if (commandTokens.length < TOKEN_COUNT) {
			logger.log(Level.WARNING, "Asked to inflate a routing command but not given enough parameters!");
			return null;
		}
		
		// Perform the inflation
		
		try {
		
			int pathID = Integer.parseInt(commandTokens[1]);
			int sourceID = Integer.parseInt(commandTokens[2]);
			return new TXRouteCommand(pathID, sourceID);
		
		} catch (Exception ex) {
			logger.log(Level.WARNING, "Ran into a problem inflating a TX routing command.", ex);
			return null;
		}
		
	}

}
