package uk.co.dlineradio.dcsiprouter.ip.commands;


import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import uk.co.dlineradio.dcsiprouter.ip.RouterIPCommand;
import uk.co.dlineradio.dcsiprouter.router.Source;
import uk.co.dlineradio.dcsiprouter.settings.Settings;

/**
 * The IP command for listing the available sources on the router.
 * This command is only sent to clients and is not processed by the server.
 * @author Marc Steele
 */

public class TXSourceCommand implements RouterIPCommand {
	
	private static Logger logger = Logger.getLogger(RouterIPCommand.class.getName());

	@Override
	public String toCommandString() {
		
		// Sanity check
		
		Map<Integer, Source> sources = Settings.get().getSources();
		if (sources == null || sources.isEmpty()) {
			logger.log(Level.WARNING, "Asked to list the available sources on the router but we couldn't find any!");
			return null;
		}
		
		// Generate the command
		
		StringBuilder sb = new StringBuilder();
		sb.append("sources ");
		
		boolean firstSource = true;
		for (Source currentSource:sources.values()) {
			if (firstSource) {
				sb.append(String.format("|%1Sd=%2$s", currentSource.getId(), currentSource.getName()));
				firstSource = false;
			} else {
				sb.append(String.format("|%1Sd=%2$s", currentSource.getId(), currentSource.getName()));
			}
		}

		sb.append("\n");
		return sb.toString();
		
	}
	
	/**
	 * Creates a new instance of the command based on the command text.
	 * @param commandText The command text.
	 */
	
	public static TXSourceCommand fromCommandText(String commandText) {
		
		// Sanity check
		
		if (commandText == null || commandText.isEmpty()) {
			logger.log(Level.WARNING, "Asked to inflate a command from the text but no text was given.");
			return null;
		}
		
		// Perform the inflation
		
		return commandText.startsWith("sources") ? new TXSourceCommand() : null;
		
	}

	@Override
	public String toString() {
		return "command requesting the list of sources";
	}

}
