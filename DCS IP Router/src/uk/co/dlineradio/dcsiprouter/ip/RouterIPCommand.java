package uk.co.dlineradio.dcsiprouter.ip;

/**
 * This is the interface that all IP commands must meet.
 * @author Marc Steele
 */

public interface RouterIPCommand {
	
	/**
	 * Converts the command to the string we'll punt out to clients.
	 * @return The command string.
	 */
	
	String toCommandString();

}
