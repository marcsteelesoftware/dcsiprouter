package uk.co.dlineradio.dcsiprouter.ip;

/**
 * The listener model used for router IP sessions.
 * @author Marc Steele
 */

public interface RouterIPSessionListener {
	
	/**
	 * Sends a command to a client.
	 * @param command The command to send to the client.
	 */
	
	void sendCommand(RouterIPCommand command);

}
