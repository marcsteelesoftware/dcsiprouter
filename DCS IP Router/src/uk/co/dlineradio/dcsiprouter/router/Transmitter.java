package uk.co.dlineradio.dcsiprouter.router;

/**
 * Represents a TX path on the matrix.
 * @author Marc Steele
 */

public class Transmitter {
	
	private int id;
	private String name = null;
	
	/**
	 * Obtains the ID number of the transmitter on the matrix.
	 * @return The ID number.
	 */
	
	public int getId() {
		return id;
	}
	
	/**
	 * Sets the ID number of the transmitter on the matrix.
	 * @param id The ID number.
	 */
	
	public void setId(int id) {
		this.id = id;
	}
	
	/**
	 * Obtains the name of the transmitter on the matrix.
	 * @return The name of the transmitter on the matrix.
	 */
	
	public String getName() {
		return name;
	}
	
	/**
	 * Sets the name of the transmitter on the matrix.
	 * @param name The name of the transmitter on the matrix.
	 */
	
	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return String.format("Router TX Path %1$d - %2$s", this.id, this.name);
	}

}
