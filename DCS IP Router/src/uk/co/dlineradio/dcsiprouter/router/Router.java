package uk.co.dlineradio.dcsiprouter.router;

/**
 * Represents a router or transmitter matrix. Specific implementations
 * will behave differently.
 * @author Marc Steele
 */

public interface Router {
	
	/**
	 * Performs any required setup process. This is router specific and before
	 * the monitoring thread starts.
	 */
	
	void setup();
	
	/**
	 * Starts the router thread running.
	 * @return TRUE if successful. Otherwise FALSE.
	 */

	boolean startRouterThread();
	
	/**
	 * Stops the router thread.
	 * @return TRUE if successful. Otherwise FALSE.
	 */
	
	boolean stopRouterThread();
	
	/**
	 * Tells the router to switch a TX path.
	 * @param source The source to switch to.
	 * @param tx_path The TX path to switch.
	 * @return TRUE if the process was successful. FALSE otherwise.
	 */
	
	boolean switchTX(int source, int tx_path);
	
	/**
	 * Finds out what the current source for a TX feed is.
	 * @param tx_feed The TX feed we want the info for.
	 * @return The source number if all is successful. Otherwise we get back -1.
	 */
	
	int currentSourceToTX(int tx_feed);
	
	/**
	 * Toggles a source onto a TX path.
	 * @param source The source we're interested in.
	 * @param tx_path The TX path we want to switch it on.
	 * @param toggle TRUE to toggle the source on, FALSE to toggle it off.
	 * @return TRUE if the process was successful, otherwise FALSE.
	 */
	
	boolean toggleSource(int source, int tx_path, boolean toggle);
	
}
