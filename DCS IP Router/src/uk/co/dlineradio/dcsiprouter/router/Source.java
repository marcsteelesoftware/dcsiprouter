package uk.co.dlineradio.dcsiprouter.router;

/**
 * Represents a source on a matrix.
 * @author Marc Steele
 */

public class Source {
	
	private int id;
	private String name = null;
	private boolean toggle = false;
	
	/**
	 * Obtains the ID number of the source on the matrix.
	 * @return The ID number.
	 */
	
	public int getId() {
		return id;
	}
	
	/**
	 * Sets the ID number of the source on the matrix.
	 * @param id The ID number.
	 */
	
	public void setId(int id) {
		this.id = id;
	}
	
	/**
	 * Obtains the name of the source on the matrix.
	 * @return The name of the source on the matrix.
	 */
	
	public String getName() {
		return name;
	}
	
	/**
	 * Sets the name of the source on the matrix.
	 * @param name The name of the source on the matrix.
	 */
	
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * Indicates if this is a toggle source or not.
	 * @return TRUE if it's a toggle source.
	 */

	public boolean isToggle() {
		return toggle;
	}
	
	/**
	 * Sets if this is a toggle source or not.
	 * @param toggle TRUE if it's a toggle source.
	 */

	public void setToggle(boolean toggle) {
		this.toggle = toggle;
	}

	@Override
	public String toString() {
		if (this.toggle) {
			return String.format("Toggle Source %1$d - %2$s", this.id, this.name);
		} else {
			return String.format("Source %1$d - %2$s", this.id, this.name);
		}
	}
	
}
