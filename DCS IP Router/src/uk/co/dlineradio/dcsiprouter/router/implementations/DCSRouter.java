package uk.co.dlineradio.dcsiprouter.router.implementations;

import gnu.io.CommPort;
import gnu.io.CommPortIdentifier;
import gnu.io.NoSuchPortException;
import gnu.io.PortInUseException;
import gnu.io.SerialPort;
import gnu.io.UnsupportedCommOperationException;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.codec.binary.Hex;

import uk.co.dlineradio.dcsiprouter.ip.RouterIPServer;
import uk.co.dlineradio.dcsiprouter.ip.commands.TXRouteCommand;
import uk.co.dlineradio.dcsiprouter.ip.commands.TXToggleCommand;
import uk.co.dlineradio.dcsiprouter.router.Router;
import uk.co.dlineradio.dcsiprouter.router.Source;
import uk.co.dlineradio.dcsiprouter.router.Transmitter;
import uk.co.dlineradio.dcsiprouter.settings.Settings;
import uk.co.dlineradio.dcsiprouter.util.BitwiseUtil;

/**
 * Represents a DCS router in the system.
 * @author Marc Steele
 */

public class DCSRouter implements Runnable, Router {
	
	private String com_port = null;
	private boolean[][] current_state = null;
	private Thread running_thread = null;
	private boolean run_thread = false;
	private Lock thread_control_lock = null;
	private ConcurrentLinkedQueue<byte[]> outbound_command_queue = null;
	
	private static final int ROW_COUNT = 4;
	private static final int SOURCE_COUNT = 8;
	private static final int SERIAL_BAUD_RATE = 2400;
	private static final int SERIAL_DATA_BITS = SerialPort.DATABITS_8;
	private static final int SERIAL_PARITY = SerialPort.PARITY_NONE;
	private static final int SERIAL_STOP_BITS = SerialPort.STOPBITS_1;
	private static final int SLEEP_TIME = 100;
	private static final int SERIAL_SLEEP_TIME = 2000;
	private static final int DCS_COMMAND_LENGTH = 5;
	private static final int DCS_COMMAND_INDEX_PREFIX = 0;
	private static final int DCS_COMMAND_INDEX_ROW = 1;
	private static final int DCS_COMMAND_INDEX_SWITCH_DATA = 2;
	private static final int DCS_COMMAND_INDEX_VOID = 3;
	private static final int DCS_COMMAND_INDEX_CHECKSUM = 4;
	private static final byte DCS_COMMAND_AUDIO_SWITCH = 0x14;
	private static final byte DCS_COMMAND_NULL = 0x00;
	private static final byte DCS_COMMAND_AUDIO_CONFIRM = 0x1F;
	private static final int DCS_COMMAND_CHECKSUM_VALUE = 0x100;
	private static final String PORT_OWNER_IDENT = "DCSIPServer";
	private static Logger logger = Logger.getLogger(DCSRouter.class.getName());
	
	public void setup() {
		
		// Clear the router state
		
		this.current_state = new boolean[ROW_COUNT][SOURCE_COUNT];
		
		for (int i = 0; i < ROW_COUNT; i++) {
			for (int j = 0; j < SOURCE_COUNT; j++) {
				this.current_state[i][j] = false;
			}
		}
		
		// Setup parameters
		
		this.thread_control_lock = new ReentrantLock();
		this.outbound_command_queue = new ConcurrentLinkedQueue<byte[]>();
		
	}
	
	/**
	 * Obtains the COM port the router is on.
	 * @return The COM port.
	 */

	public String getCom_port() {
		return com_port;
	}
	
	/**
	 * Sets the COM port the router is on.
	 * @param com_port The COM port.
	 */

	public void setCom_port(String com_port) {
		this.com_port = com_port;
	}

	@Override
	public void run() {
		
		// Setup the bits we'll need later
		
		SerialPort serial_port = null;
		InputStream serial_in = null;
		OutputStream serial_out = null;
		byte[] buffer = new byte[DCS_COMMAND_LENGTH];
		
		// Attempt to connect to the COM port
		
		try {
			
			// Start by obtaining a handle on the COM port
			
			CommPortIdentifier comm_ident = CommPortIdentifier.getPortIdentifier(this.com_port);
			if (comm_ident == null || comm_ident.isCurrentlyOwned()) {
				logger.log(Level.SEVERE, String.format("Turns out that the RS232 port %1$s for talking to the DCS switcher on either doesn't exist or someone else is hogging it!", this.com_port));
				this.stopRouterThread();
				return;
			}
			
			// Open up the port and I/O streams
			
			CommPort comm_port = comm_ident.open(PORT_OWNER_IDENT, SERIAL_SLEEP_TIME);
			if (comm_port instanceof SerialPort) {
				
				serial_port = (SerialPort) comm_port;
				serial_port.setSerialPortParams(SERIAL_BAUD_RATE, SERIAL_DATA_BITS, SERIAL_STOP_BITS, SERIAL_PARITY);
				serial_in = serial_port.getInputStream();
				serial_out = serial_port.getOutputStream();
				
				logger.log(Level.INFO, String.format("Successfully connected to DCS router on RS232 port %1$s.", this.com_port));
				
			} else {
				logger.log(Level.WARNING, String.format("While we were able to talk to COM port %1$s. It turns out that %1$s is NOT a serial port. Therefore it's impossible for DCS to be on the end of it.", com_port));
				return;
			}
			
		} catch (NoSuchPortException e) {
			logger.log(Level.SEVERE, String.format("Failed to find the RS232 port %1$s for talking to the DCS switcher.", this.com_port), e);
			this.stopRouterThread();
			return;
		} catch (PortInUseException e) {
			logger.log(Level.SEVERE, String.format("Failed to talk to the DCS router on RS232 port %1$s as someone's using the port.", this.com_port), e);
			this.stopRouterThread();
			return;
		} catch (UnsupportedCommOperationException e) {
			logger.log(Level.SEVERE, String.format("Failed to talk to the DCS router on RS232 port %1$s as the port whinged about us trying to do something it didn't support.", this.com_port), e);
			this.stopRouterThread();
			return;
		} catch (IOException e) {
			logger.log(Level.SEVERE, String.format("Ran into an IO problem trying to connect to the DCS router on RS232 port %1$s.", this.com_port), e);
			this.stopRouterThread();
			return;
		}
		
		// We now run through the TX/RX cycle. Yay!
		
		while (this.run_thread) {
			
			// Fire off the commands in the queue
			
			try {
				
				while (!this.outbound_command_queue.isEmpty()) {
					serial_out.write(this.outbound_command_queue.poll());
				}
				
			} catch (IOException e) {
				logger.log(Level.SEVERE, String.format("Ran into an IO issue firing out messages to the DCS switcher on RS232 port %1$s.", this.com_port), e);
			}
			
			// Read in messages from the router
			
			try {
				
				int length = serial_in.available();
				while (length >= DCS_COMMAND_LENGTH) {
					
					// Force the read
					
					length = serial_in.read(buffer);
					if (length == DCS_COMMAND_LENGTH) {
						
						// Check the command we saw come in
						
						if (buffer[DCS_COMMAND_INDEX_PREFIX] == DCS_COMMAND_AUDIO_CONFIRM) {
							
							// Someone's been pushing buttons on the switcher
							// Parse the raw data
							
							int row = (buffer[DCS_COMMAND_INDEX_ROW] & 0xF0) >> 4;
							boolean row_data[] = BitwiseUtil.byteToBooleanArray(buffer[DCS_COMMAND_INDEX_SWITCH_DATA]);
							
							// Sanity check
							
							if (row >= 0 && row < ROW_COUNT) {
								
								// Update the row
								
								this.current_state[row] = row_data;
								
								// Alert of TX path and codec changes
								
								Transmitter affected_tx = Settings.get().getTransmitters().get(row);
								if (affected_tx != null){
									
									for (int i = 0; i < SOURCE_COUNT; i++) {
										if (Settings.get().getSources().containsKey(i)) {
											
											Source currentSource = Settings.get().getSources().get(i);
											if (currentSource.isToggle()) {
												
												TXToggleCommand command = new TXToggleCommand(row, i, this.current_state[row][i]);
												RouterIPServer.get().queueOutboundBroadcastCommand(command);
												logger.log(Level.INFO, String.format("Sent IP command of toggling source %1$d on TX path %2$d to %3$s.", i, row, command.isToggle() ? "on" : "off"));
												
											} else if (this.current_state[row][i]) {
												
												TXRouteCommand command = new TXRouteCommand(row, i);
												RouterIPServer.get().queueOutboundBroadcastCommand(command);
												logger.log(Level.INFO, String.format("Sent IP command of routing from source %1$d to TX path %2$d.", i, row));
												
											}
											
											
										}
									}
									
								}
								
							}
							
						} else {
							
							// Saw something we don't understand
							
							logger.log(Level.INFO, String.format("Saw a message from the DCS switcher we didn't understand: 0x%1$s", new String(Hex.encodeHex(buffer))));
							
						}
						
						// Loop round
						
						length = serial_in.available();
						
					}
					
				}
				
			} catch (IOException e) {
				logger.log(Level.SEVERE, String.format("Ran into an IO reading in messages from the DCS switcher on RS232 port %1$s.", this.com_port), e);
			}
			
			// Sleep it off
			
			try {
				Thread.sleep(SLEEP_TIME);
			} catch (InterruptedException e) {
				logger.log(Level.WARNING, "DCS RS232 thread interrupted!", e);
			}
			
		}
		
		// Cleanup
		
		
		try {
			serial_in.close();
			serial_out.close();
		} catch (IOException e) {
			logger.log(Level.WARNING, String.format("Ran into an IO issue closing the streams to the DCS switcher on RS232 port %1$s.", this.com_port), e);
		}
		
		serial_port.close();
		
	}

	@Override
	public boolean startRouterThread() {
		
		this.thread_control_lock.lock();
		
		// Check we're not already running
		
		if (this.running_thread != null) {
			this.thread_control_lock.unlock();
			return false;
		}
		
		// Launch the thread
		
		this.run_thread = true;
		this.running_thread = new Thread(this);
		this.running_thread.start();
		
		this.thread_control_lock.unlock();
		return true;
		
	}

	@Override
	public boolean stopRouterThread() {
		
		this.thread_control_lock.lock();
		
		// Check we're actually running
		
		if (this.running_thread == null) {
			this.thread_control_lock.unlock();
			return false;
		} else {
			this.run_thread = false;
			this.running_thread.interrupt();
			this.running_thread = null;
			this.thread_control_lock.unlock();
			return true;
		}
		
	}

	@Override
	public boolean switchTX(int source, int tx_path) {

		// Now check against the config
		
		if (Settings.get().getTransmitters().get(tx_path) == null) {
			logger.log(Level.WARNING, String.format("Asked to switch to source %1$d on row %2$d. However, that's not a valid row on the switcher.", source, tx_path));
			return false;
		}
		
		if (Settings.get().getSources().get(source) == null) {
			logger.log(Level.WARNING, String.format("Asked to switch to source %1$d which is not a valid source number on the switcher.", source));
			return false;
		}
		
		// Change our local state
		// Don't touch sources out of our control OR toggle sources (IRN)
		
		for (int i = 0; i < SOURCE_COUNT; i++) {
			
			Source currentSource = Settings.get().getSources().get(i);
			if (currentSource != null && !currentSource.isToggle()) {
				this.current_state[tx_path][i] = (i == source);
			}
			
		}
		
		// Generate the command and punt it into the queue
		
		byte[] command = this.generateCommandForRow(tx_path);
		this.outbound_command_queue.add(command);
		logger.log(Level.INFO, String.format("Sucessfully queued command to switch TX path %1$d to source %2$d", tx_path, source));
		return true;

	}

	@Override
	public int currentSourceToTX(int tx_feed) {
		
		// Check we've got a real TX path
		
		if (Settings.get().getTransmitters().get(tx_feed) == null) {
			logger.log(Level.WARNING, String.format("Asked to get the current source for TX path %1$d but it's not a valid TX path on this switcher.", tx_feed));
			return -1;
		}
		
		// Let's hunt out the source
		
		for (int i = 0; i < SOURCE_COUNT; i++) {
			
			Source currentSource = Settings.get().getSources().get(i);
			if (currentSource != null && !currentSource.isToggle() && this.current_state[tx_feed][i]) {
				return i;
			}
			
		}
		
		// Fallback option
		
		return -1;
		
	}

	@Override
	public boolean toggleSource(int source, int tx_path, boolean toggle) {
		
		// Check it against the config
		
		if (Settings.get().getTransmitters().get(tx_path) == null) {
			logger.log(Level.WARNING, String.format("Asked to toggle source %1$d on row %2$d. However, that's not a valid row on the switcher.", source, tx_path));
			return false;
		}
		
		Source selectedSource = Settings.get().getSources().get(source);
		
		if (selectedSource == null) {
			logger.log(Level.WARNING, String.format("Asked to toggle source %1$d which is not a valid source number on the switcher.", source));
			return false;
		} else if (!selectedSource.isToggle()) {
			logger.log(Level.WARNING, String.format("Asked to toggle source %1$d on row %2$d but it's not a toggle source.", source, tx_path));
			return false;
		}
		
		// Change the local state
		
		this.current_state[tx_path][source] = toggle;
		
		// Generate the command and punt it into the queue
		
		byte[] command = this.generateCommandForRow(tx_path);
		this.outbound_command_queue.add(command);
		logger.log(Level.INFO, String.format("Sucessfully queued command to toggle source %1$d on TX path %2$d to %3$s", source, tx_path, toggle ? "on" : "off"));
		return true;
		
	}
	
	/**
	 * Generates the command for changing the state of a row.
	 * @param row The row we want to generate the command for.
	 * @return The command we generated.
	 */
	
	private byte[] generateCommandForRow(int row) {
		
		// Start by generating the basic command
				
		byte[] command = {
			0x14,
			(byte) (row << 4 | 0x0F),
			BitwiseUtil.booleanArrayToByte(this.current_state[row]),
			0x00,
			0x00
		};
				
		// Calculate the checksum
				
		int sum = 0;
		for (byte currentByte:command) {
			sum += currentByte;
		}
				
		command[DCS_COMMAND_INDEX_CHECKSUM] = (byte) (DCS_COMMAND_CHECKSUM_VALUE - sum);
		return command;
		
	}

	@Override
	public String toString() {
		return String.format("DCS Router on %1$s", this.com_port);
	}

}
