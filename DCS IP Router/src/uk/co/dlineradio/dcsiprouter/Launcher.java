package uk.co.dlineradio.dcsiprouter;

import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.cli.CommandLine;

import uk.co.dlineradio.dcsiprouter.ip.RouterIPServer;
import uk.co.dlineradio.dcsiprouter.router.Router;
import uk.co.dlineradio.dcsiprouter.router.Source;
import uk.co.dlineradio.dcsiprouter.router.implementations.DCSRouter;
import uk.co.dlineradio.dcsiprouter.settings.Settings;
import uk.co.dlineradio.dcsiprouter.util.CommandLineArguments;
import uk.co.dlineradio.dcsiprouter.util.XMLSerialiser;

/**
 * Launches the application.
 * @author Marc Steele
 */

public class Launcher {
	
	private static Logger logger = Logger.getLogger(Launcher.class.getName());
	private static final int ERROR_CODE = -1;

	/**
	 * Performs the launch process.
	 * @param args Command line arguments.
	 */
	
	public static void main(String[] args) {
		
		// Pull in the settings
		
		CommandLine commandLine = CommandLineArguments.parse(args);
		Settings settings = Settings.get(commandLine.getOptionValue("settings"));
		
		// Launch the router server
		
		Router router = settings.getRouter();
		if (router == null) {
			logger.log(Level.SEVERE, "Could not launch the server as we have no broadcast router to talk to.");
			System.exit(ERROR_CODE);
		}
		
		router.setup();
		router.startRouterThread();
		
		// Launch the IP server
		
		RouterIPServer server = RouterIPServer.get();
		server.startThread();

	}

}
