package uk.co.dlineradio.dcsiprouter.util;

import java.util.logging.Level;

import com.sun.istack.internal.logging.Logger;

/**
 * Bitwise utility methods.
 * @author Marc Steele
 */

public class BitwiseUtil {
	
	private static final int BITS_PER_BYTE = 8;
	private static Logger logger = Logger.getLogger(BitwiseUtil.class);
	
	/**
	 * Converts a boolean array to a byte.
	 * @param data The data to convert.
	 * @return The converted value.
	 */
	
	public static byte booleanArrayToByte(boolean[] data) {
		
		// Sanity check
		
		if (data == null || data.length < BITS_PER_BYTE) {
			logger.log(Level.WARNING, "Could not convert from booleans to byte as not enough data was supplied.");
			return 0x0;
		}
		
		// Perform the conversion
		
		byte response = 0x0;
		for (int i = 0; i < BITS_PER_BYTE; i++) {
			if (data[i]) {
				response = (byte) (response | (0x1 << (BITS_PER_BYTE - i - 1)));
			}
		}
			
		return response;
		
	}
	
	/**
	 * Converts a byte value to a boolean array.
	 * @param value The byte value.
	 * @return The boolean array.
	 */
	
	public static boolean[] byteToBooleanArray(byte value) {
		
		boolean[] results = new boolean[BITS_PER_BYTE];
		
		for (int i = 0; i < BITS_PER_BYTE; i++) {
			if ((value >> (BITS_PER_BYTE - i - 1) & 0x01) > 0) {
				results[i] = true;
			}
		}
		
		return results;
		
	}

}
