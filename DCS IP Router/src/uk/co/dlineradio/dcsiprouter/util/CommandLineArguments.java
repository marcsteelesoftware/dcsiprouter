package uk.co.dlineradio.dcsiprouter.util;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.cli.PosixParser;

/**
 * Provides utility methods for the command line arguments.
 * @author Marc Steele
 */

public class CommandLineArguments {
	
	private static Logger logger = Logger.getLogger(CommandLineArguments.class.getName());
	
	/**
	 * Parses the command line arguments.
	 * @param args The arguments supplied to the command.
	 * @return The parsed command line.
	 */
	
	public static CommandLine parse(String[] args) {
		
		// Setup the options
		
		Options options = new Options();
		options.addOption(
				OptionBuilder.withArgName("settings")
					.hasArg()
					.withDescription("use the given settings file")
					.create("settings")
		);
		
		// Parse the command line
		
		CommandLineParser parser = new PosixParser();
		CommandLine commandLine = null;
		
		try {
			
			commandLine = parser.parse(options, args);
			
		} catch (ParseException e) {
			logger.log(Level.SEVERE, "Ran into a problem parsing the command line arguments.", e);
			System.exit(-1);
		}
		
		// Check we've got a settings file name
		
		if (commandLine.hasOption("settings")) {
			return commandLine;
		} else {
			
			// Show the help page and quit
			
			HelpFormatter helpFormatter = new HelpFormatter();
			helpFormatter.printHelp("DCS IP Router", options);
			System.exit(-1);
			return null;
			
		}
		
	}

}
