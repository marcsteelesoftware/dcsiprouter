package uk.co.dlineradio.dcsiprouter.util;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import uk.co.dlineradio.dcsiprouter.router.Source;
import uk.co.dlineradio.dcsiprouter.router.Transmitter;
import uk.co.dlineradio.dcsiprouter.router.implementations.DCSRouter;
import uk.co.dlineradio.dcsiprouter.settings.Settings;

import com.thoughtworks.xstream.XStream;

/**
 * Provides XML serialisation services for the application.
 * @author Marc Steele
 */

public class XMLSerialiser {
	
	private static XMLSerialiser serialiser = null;
	private static Logger logger = Logger.getLogger(XMLSerialiser.class.getName());
	
	private XStream xs = null;
	
	/**
	 * Follows singleton pattern.
	 */
	
	private XMLSerialiser() {
		
		// Generic aliases
		
		this.xs = new XStream();
		this.xs.alias("settings", Settings.class);
		this.xs.alias("source", Source.class);
		this.xs.alias("transmitter", Transmitter.class);
		
		// Router specific aliases
		
		this.xs.alias("dcs", DCSRouter.class);
		
	}
	
	/**
	 * Obtains the one and only instance of the serialiser.
	 * @return The one and only instance of the serialiser.
	 */
	
	public static XMLSerialiser get() {
		
		if (serialiser == null) {
			serialiser = new XMLSerialiser();
		}
		
		return serialiser;
		
	}
	
	/**
	 * Inflates and object form an XML file.
	 * @param file_name The name of the file to inflate.
	 * @return The object assuming the process was successful. Otherwise NULL.
	 */
	
	public Object getFromFile(String file_name) {
		
		// Sanity check the file name
		
		if (file_name == null || file_name.isEmpty()) {
			logger.log(Level.SEVERE, "Asked to inflate an XML file but not actually supplied the file name. I'll leave it to you to figure out how that one went.");
			return null;
		}
		
		// Attempt the inflation
		
		try {
			
			FileInputStream file_in = new FileInputStream(file_name);
			Object inflated = this.xs.fromXML(file_in);
			file_in.close();
			return inflated;
			
		} catch (IOException e) {
			logger.log(Level.SEVERE, String.format("Ran into an IO issue trying to inflate XML file %1$s.", file_name), e);
		}
		
		return null;
		
	}
	
	/**
	 * Writes an object to an XML file.
	 * @param obj The object we're interested in.
	 * @param file_name The file to output to.
	 * @return TRUE if the process was successul. Otherwise FALSE.
	 */
	
	public boolean writeToFile(Object obj, String file_name) {
		
		// Sanity check the file name
		
		if (file_name == null || file_name.isEmpty()) {
			logger.log(Level.SEVERE, "Asked to write an object to an XML file. However, we were not told the XML file to write to.");
			return false;
		}
		
		if (obj == null) {
			logger.log(Level.SEVERE, String.format("Asked to write an object to XML file %1$s. However, as we weren't supplied an object, we're bailing out.", file_name));
			return false;
		}
		
		// Perform the serialisation
		
		try {
			
			FileOutputStream file_out = new FileOutputStream(file_name);
			this.xs.toXML(obj, file_out);
			file_out.close();
			
		} catch (FileNotFoundException e) {
			logger.log(Level.SEVERE, String.format("Turns out that XML file %1$s doesn't exist. Shouldn't be an issue as we're trying to write. But it is!", file_name), e);
			return false;
		} catch (IOException e) {
			logger.log(Level.SEVERE, String.format("Ran into an IO issue trying to write to XML file %1$s.", file_name), e);
			return false;
		}
		
		return true;
		
	}

}
