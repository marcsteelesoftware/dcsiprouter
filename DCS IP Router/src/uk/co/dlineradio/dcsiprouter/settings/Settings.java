package uk.co.dlineradio.dcsiprouter.settings;

import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import uk.co.dlineradio.dcsiprouter.router.Router;
import uk.co.dlineradio.dcsiprouter.router.Source;
import uk.co.dlineradio.dcsiprouter.router.Transmitter;
import uk.co.dlineradio.dcsiprouter.util.XMLSerialiser;

/**
 * Holds the current application settings.
 * @author Marc Steele
 */

public class Settings {
	
	private Map<Integer, Source> sources = null;
	private Map<Integer, Transmitter> transmitters = null;
	private Router router = null;
	private int port;
	
	private static Settings settings = null;
	private static Logger logger = Logger.getLogger(Settings.class.getName());
	
	/**
	 * Follows singleton pattern.
	 */
	
	private Settings() { }

	/**
	 * Obtains the current application settings.
	 * @return The current application settings.
	 */
	
	public static Settings get() {
		return Settings.settings;		
	}
	
	/**
	 * Obtains the current application settings.
	 * @param file_name The name of the settings file.
	 * @return The current application settings.
	 */
	
	public static Settings get(String file_name) {
		
		if (Settings.settings == null) {
			
			// Inflate from XML
			
			Settings.settings = (Settings) XMLSerialiser.get().getFromFile(file_name);
			
			// If it's all gone horribly wrong. Fall back to defaults.
			
			if (Settings.settings == null) {
				Settings.settings = new Settings();
				logger.log(Level.WARNING, "Couldn't load settings from XML file so fallen back to defaults. This means we won't actually see anything work.");
			}
			
		}
		
		return Settings.settings;
		
	}
	
	/**
	 * Obtains the sources we want the system to control.
	 * @return The sources.
	 */

	public Map<Integer, Source> getSources() {
		return sources;
	}
	
	/**
	 * Sets the sources we want the system to control.
	 * @param source The sources.
	 */

	public void setSources(Map<Integer, Source> source) {
		this.sources = source;
	}
	
	/**
	 * Obtains the TX feeds we want the system to control.
	 * @return The TX feeds.
	 */

	public Map<Integer, Transmitter> getTransmitters() {
		return transmitters;
	}
	
	/**
	 * Sets the TX feeds we want the system to control.
	 * @param transmitters The TX feeds.
	 */

	public void setTransmitters(Map<Integer, Transmitter> transmitters) {
		this.transmitters = transmitters;
	}
	
	/**
	 * Obtains the router we want the system to interface with.
	 * @return The router we want the system to interface with.
	 */

	public Router getRouter() {
		return router;
	}
	
	/**
	 * Sets the router we want the system to interface with.
	 * @param router The router we want the system to interface with.
	 */

	public void setRouter(Router router) {
		this.router = router;
	}
	
	/**
	 * Obtains the port number we want to run the IP server on.
	 * @return The port number.
	 */

	public int getPort() {
		return port;
	}
	
	/**
	 * Sets the port number we want to run the IP server on.
	 * @param port The port number.
	 */

	public void setPort(int port) {
		this.port = port;
	}

}
